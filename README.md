## Project Ready Template
A Project boilerplate with handy features.

### What's inside
  1. BrowserSync - Sync file changes and interactions across many devices.
  2. Babel - convert ECMAScript 2015+ code into a backwards compatible version of JavaScript in current and older browsers or environments.
  3. Sass - Convert Sass/Scss files to css with sourcemaps.
  4. Autoprefixer - No need to add vendor prefixers, Autoprefixer does that for you.
  5. Semi 7-1 - Inspired by the 7-1 Architecture(by [Hugo Giraudel](https://sass-guidelin.es/#the-7-1-pattern)) for a more organized and modularized styles.

### How to use
```
> git clone https://renz_tullao@bitbucket.org/renz_tullao/project-ready-boilerplate.git
> npm install
> gulp 
```

### FAQ
#### OS Issues
I'm having some error on OS Compatibility
- Try running

```
> npm rebuild
```
Then try to run `gulp watch` again.

### Screenshots
#### Babel
![babel](https://snag.gy/0tr1fY.jpg)

#### IE 8 Below msg error
![ie8](https://snag.gy/d3aA5r.jpg)
