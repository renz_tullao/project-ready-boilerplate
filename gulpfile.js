var gulp = require('gulp');
var sass = require('gulp-sass');
var bs = require('browser-sync').create();
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var cssnano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');

var plumber = require('gulp-plumber');
var gutil = require('gulp-util');

var paths = {
  scss: 'assets/scss/',
  css: 'assets/css/',
  js: 'assets/js/',
  jsDist: 'assets/js/dist/'
};

// var gulp_src = gulp.src;
// gulp.src = function () {
//   return gulp_src.apply(gulp, arguments)
//     .pipe(plumber(function (error) {
//       gutil.log(gutil.colors.red('Error (' + error.plugin + '): ' + error.message));
//       this.emit('end');
//     })
//     );
// };

gulp.task('babel', done => {
  gulp.src(`${paths.js}main.js`)
    .pipe(babel({
      presets: ['@babel/preset-env']
    }))
    .pipe(rename(function(path){
      path.basename = 'index';
    }))
    .pipe(gulp.dest(`.${paths.jsDist}`))
  done();
})

gulp.task('sass', done => {
  return gulp
    .src(`${paths.scss}main.scss`)
    .pipe(sourcemaps.init())
    .pipe(sass({errLogToConsole: true, outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer())
    // Remove comment below for Production
    // .pipe(rename({ suffix: '.min' }))
    // .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(paths.css))
    .pipe(bs.reload({
      stream: true
    }))

  done();
});
gulp.task('browserSync', done => {
  bs.init({
    server: {
      baseDir: './',
    },
    cors: true,
  })
  done();
})

gulp.task('default', gulp.series('browserSync', 'sass', 'babel', function () {
  gulp.watch(`${paths.scss}base/*.scss`, gulp.series('sass'));
  gulp.watch(`${paths.scss}components/*.scss`, gulp.series('sass'));
  gulp.watch(`${paths.scss}layout/*.scss`, gulp.series('sass'));
  gulp.watch(`${paths.scss}pages/*.scss`, gulp.series('sass'));
  gulp.watch(`${paths.scss}*.scss`, gulp.series('sass'));
  gulp.watch('*.html').on('change', bs.reload);
  gulp.watch(`${paths.js}*.js`, gulp.series('babel')).on('reload', bs.reload);
}));